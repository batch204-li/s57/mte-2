let collection = [];

// Write the queue functions below.
// Usage of JS ARRAY METHODS (push, pop, shift, unshift, etc) is not allowed
//Properties (like .lenght) are allowed.

// Show Queue
function print() {
   return collection;
}

// add an element to the queue
function enqueue(element) {
  collection[collection.length] = element;
  return collection

}

// remove an element to the queue
function dequeue() {
   for (let i = 0; i < collection.length - 1; i++) {
       collection[i] = collection[i + 1];
     };
     --collection.length;
     return collection
}

// display the element at the front of queue
function front() {
    return collection[0];
}

// display the size of queue
function size() {
    return collection.length;
}

// check if queue is empty
function isEmpty() {
    return collection.length == 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};